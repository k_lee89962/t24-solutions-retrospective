## Retrospective

---

**Jisoo Lee, Julian Shuker, Jayden Wood**

Follow the instructions to use the source code to simulate application in virtual mobile device.

**Requirement:**  
- Android Studio  
- Java Environment Setup  

**Steps:**  
1. Download the source code, and open it on Android Studio  
2. On the top bar, select the virtual device to run the application on. If there is none downloaded, download a virtual mobile device through Anroid Studio AVD Manager.  
3. Click Run button (Arrow button) at top bar to run the application throught virtual device.  
