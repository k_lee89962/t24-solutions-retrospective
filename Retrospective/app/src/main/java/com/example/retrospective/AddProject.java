package com.example.retrospective;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class AddProject extends AppCompatActivity {

    // Variables used for all the functions
    public static final String TEAM_NAME = "teamName";
    public static final String PMO_NAME = "pmoName";
    EditText editTextProjectName;
    Button buttonAddProject;
    DatabaseReference databaseProject;
    List<Project> projectList;
    Project project;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_project);
        getSupportActionBar().setTitle("Add Project");

        // Get content from layouts
        editTextProjectName = findViewById(R.id.editTextProjectName);
        buttonAddProject = findViewById(R.id.buttonAddProject);

        // get firebase reference and objects for application
        databaseProject = FirebaseDatabase.getInstance().getReference("Project");
        projectList = new ArrayList<Project>();
        project = new Project();

        // get variables from previous activity
        Intent intent = getIntent();
        final String teamName = intent.getStringExtra(TeamInfo.TEAM_NAME);
        final String pmoName = intent.getStringExtra(TeamInfo.PMO_NAME);

        // button to add project
        buttonAddProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String projectName = editTextProjectName.getText().toString().trim();
                // if text is null
                if (TextUtils.isEmpty(projectName)) {
                    printEmptyErrorMessage();
                }
                else {
                    boolean found = false;
                    // go through list and see if the project name exists already
                    for (Project x : projectList) {
                        if (x.getProjectName().compareTo(projectName) == 0) {
                            found = true;
                            break;
                        }
                    }
                    // if existing project with same name is found, print error
                    if (found) {
                        printExistingNameErrorMessage();
                    }
                    // else, create project and move to new activity
                    else {
                        project.setProjectName(projectName);
                        project.setTeamName(teamName);
                        project.declareList();
                        databaseProject.child(projectName).setValue(project);
                        successfulAddition(teamName);
                        // new activity with extras
                        Intent intent = new Intent(getApplicationContext(), TeamInfo.class);
                        intent.putExtra(TEAM_NAME, teamName);
                        intent.putExtra(PMO_NAME, pmoName);
                        startActivity(intent);
                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    }
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        // use database reference to read in projects in firebase database
        databaseProject.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                projectList.clear();

                for (DataSnapshot projectSnapshot : dataSnapshot.getChildren()) {
                    Project tempProject = projectSnapshot.getValue(Project.class);
                    projectList.add(tempProject);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    // Error messages
    public void printEmptyErrorMessage() {
        Toast.makeText(this, "You have to choose the name for your project.", Toast.LENGTH_LONG).show();
    }

    public void printExistingNameErrorMessage() {
        Toast.makeText(this, "This name already exists within the list of projects.", Toast.LENGTH_LONG).show();
    }

    public void successfulAddition(String teamName) {
        Toast.makeText(this, "Successfully created a new project.", Toast.LENGTH_LONG).show();

    }
}
