package com.example.retrospective;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class AddTeamMembers extends AppCompatActivity {

    // Variables used for all the functions
    public static final String TEAM_NAME = "teamName";
    ListView listViewTeamMembers;
    DatabaseReference databaseMembers, databaseTeams;
    List<Member> memberList;
    Member member;
    List<Team> teamList;
    Team team;

    String teamName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_team_members);
        getSupportActionBar().setTitle("Add Team Member");

        // Get content from layouts
        listViewTeamMembers = findViewById(R.id.listViewTeamMembers);
        databaseMembers = FirebaseDatabase.getInstance().getReference("Member");
        databaseTeams = FirebaseDatabase.getInstance().getReference("Team");
        memberList = new ArrayList<Member>();
        teamList = new ArrayList<Team>();
        team = new Team();

        // get variables from previous activity
        Intent intent = getIntent();
        teamName = intent.getStringExtra(TeamInfo.TEAM_NAME);

        // click listener for the list of team members
        listViewTeamMembers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                member = memberList.get(position);
                member.addTeam(teamName);

                // add the selected member from list to the team list
                for (Team x : teamList) {
                    if (x.getTeamName().compareTo(teamName) == 0) {
                        team = x;
                        break;
                    }
                }
                team.addTeamMember(member);

                // add to firebase database and open new activity
                databaseMembers.child(member.getUsername()).setValue(member);
                databaseTeams.child(teamName).setValue(team);
                openTeamInfo();

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        // read in firebase database to see existing team list
        databaseTeams.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                teamList.clear();

                for (DataSnapshot teamSnapshot : dataSnapshot.getChildren()) {
                    Team tm = teamSnapshot.getValue(Team.class);
                    teamList.add(tm);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        // read in firebase database to look for members that isn't in the selected team
        databaseMembers.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                memberList.clear();

                boolean found;

                for (DataSnapshot memberSnapshot : dataSnapshot.getChildren()) {
                    found = false;
                    Member mem = memberSnapshot.getValue(Member.class);
                    // check for empty team list
                    if (mem.getTeamList() != null) {
                        if (mem.getTeamList() != null) {
                            for (String x : mem.getTeamList()) {
                                if (x.compareTo(teamName) == 0) {
                                    found = true;
                                    break;
                                }
                            }
                        }
                        if (!found) {
                            memberList.add(mem);
                        }
                    }
                    else {
                        memberList.add(mem);
                    }
                }

                MemberList adapter = new MemberList(AddTeamMembers.this, memberList);
                listViewTeamMembers.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    // opens team info activity
    public void openTeamInfo() {
        Toast.makeText(this, "Added Member", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, MemberInfo.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
}
