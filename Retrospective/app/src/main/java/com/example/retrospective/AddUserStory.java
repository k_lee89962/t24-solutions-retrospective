package com.example.retrospective;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class AddUserStory extends AppCompatActivity {

    // Variables used for all the functions
    public static final String TEAM_NAME = "teamName";
    public static final String PMO_NAME = "pmoName";
    EditText editTextUserStory, editTextUserStoryEstimate;
    Button buttonAddUserStory;
    DatabaseReference databaseProject;
    String teamName, pmoName;
    List<UserStory> userStoryList;
    MyApplication m = MyApplication.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user_story);
        getSupportActionBar().setTitle("Add User Story");

        // Get content from layouts
        editTextUserStory = findViewById(R.id.editTextUserStory);
        editTextUserStoryEstimate = findViewById(R.id.editTextUserStoryEstimate);
        buttonAddUserStory = findViewById(R.id.buttonAddUserStory);

        // get firebase reference and objects for application
        databaseProject = FirebaseDatabase.getInstance().getReference("Project");
        userStoryList = new ArrayList<UserStory>();

        // get variables from previous activity
        Intent intent = getIntent();
        teamName = intent.getStringExtra(ProjectInfo.TEAM_NAME);
        pmoName = intent.getStringExtra(ProjectInfo.PMO_NAME);

        // button to add user story to selected project
        buttonAddUserStory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // if input is not a number, print error message
                if (!isInt(editTextUserStoryEstimate.getText().toString().trim())) {
                    printErrorMessage();
                }
                else {
                    String task = editTextUserStory.getText().toString().trim();
                    int estimate = Integer.parseInt(editTextUserStoryEstimate.getText().toString().trim());
                    boolean found = false;

                    // find if there is user story with same task exists
                    for (UserStory x : userStoryList) {
                        if (x.getTask().compareTo(task) == 0) {
                            found = true;
                            break;
                        }
                    }
                    // if found, error
                    if (found) {
                        printExistingUserStoryErrorMessage();
                    }
                    // else, add to firebase database
                    else {
                        m.getSelectedProject().addUserStories(task, estimate);
                        databaseProject.child(m.getSelectedProject().getProjectName()).setValue(m.getSelectedProject());
                        successfulAddition();
                    }
                }
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();

        // read firebase database to get user stories from selected project
        databaseProject.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                userStoryList.clear();

                for (DataSnapshot userStorySnapshot : dataSnapshot.getChildren()) {
                    Project project = userStorySnapshot.getValue(Project.class);

                    if (project.getTeamName().compareTo(m.getSelectedProject().getTeamName()) == 0 && project.getUserStories() != null) {
                        for (UserStory x : project.getUserStories()) {
                            userStoryList.add(x);
                        }
                        break;
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    // checks if the string is num or not
    public boolean isInt(String num) {
        if (num == null) return false;
        try {
            int i = Integer.parseInt(num);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    // error messages
    public void printErrorMessage() {
        Toast.makeText(this, "You have to type in proper input.", Toast.LENGTH_LONG).show();
    }

    public void printExistingUserStoryErrorMessage() {
        Toast.makeText(this, "This user story already exists within your list of user stories.", Toast.LENGTH_LONG).show();
    }

    // open new activity
    public void successfulAddition() {
        Toast.makeText(this, "User story have been added.", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(AddUserStory.this, ProjectInfo.class);
        intent.putExtra(TEAM_NAME, teamName);
        intent.putExtra(PMO_NAME, pmoName);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

    }
}
