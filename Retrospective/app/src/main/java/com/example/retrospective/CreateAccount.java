package com.example.retrospective;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class CreateAccount extends AppCompatActivity {

    // Variables used for all the functions
    EditText editFirstName, editLastName, editUsername, editPassword, editConfirmPassword, editEmail, editRole;
    Button buttonCreate;
    DatabaseReference databaseMember;
    Member member;
    MyApplication m = MyApplication.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);
        getSupportActionBar().setTitle("Create Account");

        // Get content from layouts
        editFirstName = findViewById(R.id.editFirstName);
        editLastName = findViewById(R.id.editLastName);
        editUsername = findViewById(R.id.editUsername);
        editPassword = findViewById(R.id.editPassword);
        editConfirmPassword = findViewById(R.id.editConfirmPassword);
        editEmail = findViewById(R.id.editEmail);
        editRole = findViewById(R.id.editRole);
        buttonCreate = findViewById(R.id.buttonCreate);

        // get firebase reference and objects for application
        member = new Member();
        databaseMember = FirebaseDatabase.getInstance().getReference("Member");
        m.declareList();

        // button that creates account to use this application
        buttonCreate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                boolean found = false;

                // checks if username already exists
                for (Member x : m.getMemberList()) {
                    if (x.getUsername().compareTo(editUsername.getText().toString().trim()) == 0) {
                        found = true;
                    }
                }
                // if found, then error message
                if (found) {
                    Toast toast2 = Toast.makeText(getApplicationContext(), "Username already exists", Toast.LENGTH_LONG);
                    toast2.show();
                }
                // add a member to firebase database, and open new activity
                else {
                    if (editPassword.getText().toString().trim().compareTo(editConfirmPassword.getText().toString().trim()) == 0) {
                        member.setUsername(editUsername.getText().toString().trim());
                        member.setEmail(editEmail.getText().toString().trim());
                        member.setFirstName(editFirstName.getText().toString().trim());
                        member.setUsername(editUsername.getText().toString().trim());
                        member.setLastName(editLastName.getText().toString().trim());
                        member.setPassword(editPassword.getText().toString().trim());
                        member.setRole(editRole.getText().toString().trim());
                        member.declareList();

                        databaseMember.child(editUsername.getText().toString().trim()).setValue(member);
                        Toast toast2 = Toast.makeText(getApplicationContext(), "Account Created. Log on to your account", Toast.LENGTH_LONG);
                        toast2.show();

                        openLogin();
                    }
                    else {
                        Toast toast2 = Toast.makeText(getApplicationContext(), "Password does not match confirm password", Toast.LENGTH_LONG);
                        toast2.show();
                    }
                }
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();

        // read in firebase database to get list of members
        databaseMember.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                m.clearList();

                for (DataSnapshot memberSnapshot: dataSnapshot.getChildren()) {
                    Member mem = memberSnapshot.getValue(Member.class);

                    m.getMemberList().add(mem);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    // opens new activity
    public void openLogin() {
        Intent intent = new Intent(this, LoginAccount.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
}
