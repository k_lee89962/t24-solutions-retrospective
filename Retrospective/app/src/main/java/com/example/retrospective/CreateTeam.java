package com.example.retrospective;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class CreateTeam extends AppCompatActivity {

    // Variables used for all the functions
    EditText textTeamName;
    Button  buttonCreateTeam;
    DatabaseReference databaseTeams, databaseMembers;
    MyApplication m = MyApplication.getInstance();
    List<Team> existingTeams;
    Team newTeam;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_team);
        getSupportActionBar().setTitle("Create Team");

        // Get content from layouts
        textTeamName = findViewById(R.id.editTextTeamName);
        buttonCreateTeam = findViewById(R.id.buttonAddTeam);

        // get firebase reference and objects for application
        databaseTeams = FirebaseDatabase.getInstance().getReference("Team");
        databaseMembers = FirebaseDatabase.getInstance().getReference("Member");
        newTeam = new Team();
        existingTeams = new ArrayList<Team>();

        // button to create a team and add to databaseq
        buttonCreateTeam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String teamName = textTeamName.getText().toString().trim();
                boolean found = false;

                // if input is empty, print error message
                if (teamName.compareTo("") == 0) {
                    printEmptyErrorMessage();
                }
                // check if team with same name exists
                for (Team x : existingTeams) {
                    if (teamName.compareTo(x.getTeamName()) == 0) {
                        found = true;
                        break;
                    }
                }
                // if there exists team with same name, print error message
                if (found) {
                    printExistingNameErrorMessage();
                }
                // else, add team to database
                else {
                    newTeam.setPmo(m.getLoggedInMember().getUsername());
                    newTeam.setTeamMembers(new ArrayList<Member>());
                    newTeam.setTeamName(teamName);
                    newTeam.addTeamMember(m.getLoggedInMember());

                    m.getLoggedInMember().addTeam(teamName);
                    databaseMembers.child(m.getLoggedInMember().getUsername()).setValue(m.getLoggedInMember());
                    databaseTeams.child(newTeam.getTeamName()).setValue(newTeam);

                    // move to new activity
                    successfulAddition();

                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        // read in list of teams in database
        databaseTeams.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot memberSnapshot: dataSnapshot.getChildren()) {
                    Team team = memberSnapshot.getValue(Team.class);

                    existingTeams.add(team);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    // error messages
    public void printEmptyErrorMessage() {
        Toast.makeText(this, "You have to choose the name for your team.", Toast.LENGTH_LONG).show();
    }

    public void printExistingNameErrorMessage() {
        Toast.makeText(this, "This name already exists within your list of teams.", Toast.LENGTH_LONG).show();
    }

    // open up new activity
    public void successfulAddition() {
        Toast.makeText(this, "Successfully created a new team.", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, MemberInfo.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
}
