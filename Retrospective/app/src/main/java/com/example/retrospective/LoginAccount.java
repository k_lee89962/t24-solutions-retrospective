package com.example.retrospective;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class LoginAccount extends AppCompatActivity {

    // Variables used for all the functions
    EditText editUsername, editPassword;
    Button buttonLogin;
    DatabaseReference databaseMembers;
    MyApplication m = MyApplication.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_account);
        getSupportActionBar().setTitle("Login Account");

        // Get content from layouts
        editUsername = findViewById(R.id.editUsername);
        editPassword = findViewById(R.id.editPassword);
        buttonLogin = findViewById(R.id.buttonLogin);

        // get firebase reference and objects for application
        databaseMembers = FirebaseDatabase.getInstance().getReference("Member");
        m.declareList();

        // button to login to application
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        // access in to member list in firebase database
        databaseMembers.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                m.clearList();

                for (DataSnapshot memberSnapshot: dataSnapshot.getChildren()) {
                    Member member = memberSnapshot.getValue(Member.class);

                    m.getMemberList().add(member);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    // function that checks over the inputs, and see if the account exists  within database to log in
    private void login() {
        String username = editUsername.getText().toString().trim();
        String password = editPassword.getText().toString().trim();
        Member member = null;
        boolean found = false;

        // if input is not empty
        if (!TextUtils.isEmpty(username)) {
            // go through every existing members to see if there is matching username
            for (Member x : m.getMemberList()) {
                if (x.getUsername().compareTo(username) == 0) {
                    member = x;
                    break;
                }
            }

            // member is found
            if (member != null) {
                // if password matches, log in
                if (member.getPassword().compareTo(password) == 0) {
                    m.setLoggedInMember(member);
                    Toast.makeText(this, "Successfully Logged in", Toast.LENGTH_LONG).show();
                    openMemberInfo();
                }
                // if not, error message
                else {
                    Toast.makeText(this, "Password is incorrect", Toast.LENGTH_LONG).show();
                }
            }
            // member is not found, print error
            else {
                Toast.makeText(this, "User does not exist", Toast.LENGTH_LONG).show();
            }
        }
        // username is not entered, print error
        else {
            Toast.makeText(this, "You should enter a name", Toast.LENGTH_LONG).show();
        }
    }

    // open new activity
    public void openMemberInfo() {
        Intent intent = new Intent(this, MemberInfo.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
