package com.example.retrospective;

import java.util.List;
import java.util.ArrayList;

// Class that will handle information regarding the team member
public class Member {

    // variables
    private String FirstName;
    private String LastName;
    private String Password;
    private String Email;
    private String Role;
    private String Username;
    private List<String> TeamList;

    public Member() {
    }

    // getter and setters
    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getRole() {
        return Role;
    }

    public void setRole(String role) {
        Role = role;
    }

    public String getUsername() { return Username; }

    public void setUsername(String username) { Username = username; }

    public List<String> getTeamList() { return TeamList; }

    public void setTeamList(List<String> teamList) { this.TeamList = teamList; }

    // declare a array list for team list
    public void declareList() { TeamList = new ArrayList<String>(); }

    // add team to the current team list
    public void addTeam(String team) {
        if (this.TeamList == null) declareList();
        TeamList.add(team);
    }

}
