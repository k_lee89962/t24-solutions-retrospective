package com.example.retrospective;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;
import java.util.ArrayList;

public class MemberInfo extends AppCompatActivity {

    // Variables used for all the functions
    public static final String TEAM_NAME = "teamName";
    public static final String PMO_NAME = "pmoName";
    public static final Team TEAM = new Team();
    TextView textFullName, textLoggedInUsername, textLoggedInEmail, textLoggedInRole;
    Button buttonLogOut, buttonAddTeam;
    DatabaseReference databaseMembers, databaseTeams;
    ListView listViewTeams;
    MyApplication m = MyApplication.getInstance();
    List<Team> tempList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_info);
        getSupportActionBar().setTitle("Member Information");

        // Get content from layouts
        textFullName = findViewById(R.id.textFullName);
        textLoggedInUsername = findViewById(R.id.textLoggedInUsername);
        textLoggedInEmail = findViewById(R.id.textLoggedInEmail);
        textLoggedInRole = findViewById(R.id.textLoggedInRole);
        buttonLogOut = findViewById(R.id.buttonLogOut);
        buttonAddTeam = findViewById(R.id.buttonAddTeam);
        listViewTeams = (ListView) findViewById(R.id.listViewTeam);

        // get firebase reference and objects for application
        tempList = new ArrayList<Team>();
        databaseTeams = FirebaseDatabase.getInstance().getReference("Team");
        databaseMembers = FirebaseDatabase.getInstance().getReference("Member");

        // set the content of activity
        String fullName = m.getLoggedInMember().getFirstName() + " " + m.getLoggedInMember().getLastName();
        textFullName.setText(fullName);
        textLoggedInUsername.setText(m.getLoggedInMember().getUsername());
        textLoggedInEmail.setText(m.getLoggedInMember().getEmail());
        textLoggedInRole.setText(m.getLoggedInMember().getRole());

        // button to log out from the application
        buttonLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                databaseMembers.child(m.getLoggedInMember().getUsername()).setValue(m.getLoggedInMember());
                m.logout();
                openLoginCreate();
            }
        });

        // button to create a team
        buttonAddTeam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCreateTeam();
            }
        });

        // click listener to select a team associated with logged in account
        listViewTeams.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Team team = tempList.get(position);

                Intent intent = new Intent(getApplicationContext(), TeamInfo.class);

                intent.putExtra(TEAM_NAME, team.getTeamName());
                intent.putExtra(PMO_NAME, team.getPmo());

                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        // read in teams from firebase
        databaseTeams.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                tempList.clear();

                for (DataSnapshot teamSnapshot : dataSnapshot.getChildren()) {

                    Team team = teamSnapshot.getValue(Team.class);

                    // if the team name matches the team name from the logged in members team list, add them to display
                    if (m.getLoggedInMember().getTeamList() != null) {
                        for (String x : m.getLoggedInMember().getTeamList()) {
                            if (x.compareTo(team.getTeamName()) == 0) {
                                tempList.add(team);
                            }
                        }
                    }
                }

                TeamList adapter = new TeamList(MemberInfo.this, tempList);
                listViewTeams.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    // open new activity
    public void openCreateTeam() {
        Intent intent = new Intent(this, CreateTeam.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public void openLoginCreate() {
        Toast.makeText(this, "Successfully Logged Out", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, Login_Create.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
}
