package com.example.retrospective;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

// Array adapter class that helps display the list of members on activity
public class MemberList extends ArrayAdapter<Member> {

    // variables
    private Activity context;
    private List<Member> memberList;
    private MyApplication m = MyApplication.getInstance();

    // constructor
    public MemberList(Activity context, List<Member> memberList) {
        super(context, R.layout.list_layout, memberList);
        this.context = context;
        this.memberList = memberList;
    }

    // get the list view of the members
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.list_layout, null, true);

        // get the content of the activity
        TextView textMemberName = listViewItem.findViewById(R.id.textMainName);
        TextView textRoleName = listViewItem.findViewById(R.id.textSubName);

        Member member = memberList.get(position);

        // fill in the content
        textMemberName.setText(member.getFirstName() + " " + member.getLastName() + " (" + member.getUsername() + ")");
        textRoleName.setText("Role: " + member.getRole());

        return listViewItem;
    }

}
