package com.example.retrospective;

import android.app.Application;
import java.util.ArrayList;
import java.util.List;

// class that handles global variable using the singleton pattern
public class MyApplication extends Application {

    private static MyApplication instance;
    private List<Member> memberList;
    private Member loggedInMember;
    private Project selectedProject;

    private MyApplication() {}

    // singleton pattern
    public static synchronized  MyApplication getInstance() {
        if (instance == null) {
            instance = new MyApplication();
        }
        return instance;
    }

    // getter and setters
    public List<Member> getMemberList() {
        return memberList;
    }

    public void setMemberList(List<Member> memberList) {
        this.memberList = memberList;
    }

    public Member getLoggedInMember() {
        return loggedInMember;
    }

    public void setLoggedInMember(Member loggedInMember) {
        this.loggedInMember = loggedInMember;
    }

    public Project getSelectedProject() {
        return selectedProject;
    }

    public void setSelectedProject(Project selectedProject) {
        this.selectedProject = selectedProject;
    }

    // declares the member list
    public void declareList() {
        this.memberList = new ArrayList<Member>();
    }

    // clears the member list
    public void clearList() {
        this.memberList.clear();
    }

    // clears out the member list and logged in member
    public void logout() {
        this.memberList = null;
        this.loggedInMember = null;
    }

}
