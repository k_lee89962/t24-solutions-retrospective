package com.example.retrospective;

import java.util.ArrayList;
import java.util.List;

// Class that will handle information regarding the team projects
public class Project {

    // variables
    private String projectName;
    private String teamName;
    private List<UserStory> UserStories;

    // getter and setters
    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public List<UserStory> getUserStories() {
        return UserStories;
    }

    public void setUserStories(List<UserStory> userStories) {
        UserStories = userStories;
    }

    // declares array list for UserStories
    public void declareList() {
        this.UserStories = new ArrayList<UserStory>();
    }

    // adds user story to our list
    public void addUserStories(String task, int estimateRelative) {
        if (UserStories == null) declareList();
        UserStory us = new UserStory();
        us.setTask(task);
        us.setAssignedMember("");
        us.setRelativeEstimate(estimateRelative);
        us.setEstimateHours(0);
        us.setActualHours(0);
        us.setStatus("ONGOING");
        us.setSprintPhase(1);
        UserStories.add(us);
    }

    // returns to total estimate of all the user stories
    public int getTotalEstimate() {
        int result = 0;
        if (UserStories != null ) {
            for (UserStory x : UserStories) {
                result += x.getRelativeEstimate();
            }
        }
        return result;
    }

    // returns total estimate of completed user stories
    public int getCompletedEstimate() {
        int result = 0;
        for (UserStory x : UserStories) {
            if (x.getStatus().compareTo("COMPLETE") == 0)  result += x.getRelativeEstimate();
        }
        return result;
    }

    // returns total estimate of user stories that are still on going
    public int getOngoingEstimate() {
        int result = 0;
        for (UserStory x : UserStories) {
            if (x.getStatus().compareTo("ONGOING") == 0)  result += x.getRelativeEstimate();
        }
        return result;
    }
}
