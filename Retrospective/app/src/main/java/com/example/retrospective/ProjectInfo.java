package com.example.retrospective;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ProjectInfo extends AppCompatActivity {

    // Variables used for all the functions
    public static final String TEAM_NAME = "teamName";
    public static final String PMO_NAME = "pmoName";
    public static final String POSITION = "0";
    TextView textProjectName;
    ListView listViewUserStories;
    Button buttonAddUserStory;
    DatabaseReference databaseProject;
    List<UserStory> userStories;
    String teamName, pmoName;
    MyApplication m = MyApplication.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_info);
        getSupportActionBar().setTitle("Project Information");

        // Get content from layouts
        textProjectName = findViewById(R.id.textProjectName);
        listViewUserStories = findViewById(R.id.listViewUserStories);
        buttonAddUserStory = findViewById(R.id.buttonAddUserStory);

        // get firebase reference and objects for application
        databaseProject = FirebaseDatabase.getInstance().getReference("Project");
        userStories = new ArrayList<UserStory>();

        // get variables from previous activity
        Intent intent = getIntent();
        teamName = intent.getStringExtra(TeamInfo.TEAM_NAME);
        pmoName = intent.getStringExtra(TeamInfo.PMO_NAME);

        // set the content of activity
        textProjectName.setText(m.getSelectedProject().getProjectName());

        // button to add user story to project
        buttonAddUserStory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAddUserStory();
            }
        });

        // click listener to select from the list of user stories within project
        listViewUserStories.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                openUserStoryInfo(String.valueOf(position));
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        // read in projects from firebase
        databaseProject.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                userStories.clear();

                for (DataSnapshot projectSnapshot : dataSnapshot.getChildren()) {

                    Project project = projectSnapshot.getValue(Project.class);
                    // add all projects from firebase that matches selected project
                    if (project.getProjectName().compareTo(m.getSelectedProject().getProjectName()) == 0) {
                        if (project.getUserStories() != null) {
                            for (UserStory x : project.getUserStories()) {
                                userStories.add(x);
                            }
                        }
                    }
                }

                UserStoryList adapter = new UserStoryList(ProjectInfo.this, userStories);
                listViewUserStories.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    // open new activity
    public void openUserStoryInfo(String position) {

        Intent intent = new Intent(ProjectInfo.this, UserStoryInfo.class);
        intent.putExtra(TEAM_NAME, teamName);
        intent.putExtra(PMO_NAME, pmoName);
        intent.putExtra(POSITION, position);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

    }

    public void openAddUserStory() {

        Intent intent = new Intent(ProjectInfo.this, AddUserStory.class);
        intent.putExtra(TEAM_NAME, teamName);
        intent.putExtra(PMO_NAME, pmoName);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

    }

}
