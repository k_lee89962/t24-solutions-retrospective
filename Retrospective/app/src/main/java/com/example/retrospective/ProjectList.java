package com.example.retrospective;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

// Array adapter class that helps display the list of projects on activity
public class ProjectList extends ArrayAdapter<Project> {

    // variables
    private Activity context;
    private List<Project> projectList;

    // constructor
    public ProjectList(Activity context, List<Project> projectList) {
        super(context, R.layout.list_layout, projectList);
        this.context = context;
        this.projectList = projectList;
    }

    // get the list view of the projects
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.list_layout, null, true);

        // get the content of the activity
        TextView textProjectName = listViewItem.findViewById(R.id.textMainName);
        TextView textProjectEstimate = listViewItem.findViewById(R.id.textSubName);

        Project project = projectList.get(position);

        // fill in the content
        textProjectName.setText(project.getProjectName());
        textProjectEstimate.setText("Estimate Point: " + Integer.toString(project.getTotalEstimate()));

        return listViewItem;
    }

}
