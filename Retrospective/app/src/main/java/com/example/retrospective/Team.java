package com.example.retrospective;

import java.util.ArrayList;
import java.util.List;

// Class that will handle information regarding the team itself
public class Team {
    // variables
    private String teamName;
    private String pmo;
    private List<Member> teamMembers;

    public Team() {}

    // getters and setters
    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getPmo() {
        return pmo;
    }

    public void setPmo(String pmo) {
        this.pmo = pmo;
    }

    public List<Member> getTeamMembers() {
        return teamMembers;
    }

    public void setTeamMembers(List<Member> teamMembers) {
        this.teamMembers = teamMembers;
    }

    // declare ArrayList for teamMembers
    public void declareList() {
        this.teamMembers = new ArrayList<Member>();
    }

    // add a member to teamMember list
    public void addTeamMember(Member member) {
        if (this.teamMembers == null) declareList();
        this.teamMembers.add(member);
    }
}
