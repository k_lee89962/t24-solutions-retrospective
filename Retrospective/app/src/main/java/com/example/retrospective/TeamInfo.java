package com.example.retrospective;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class TeamInfo extends AppCompatActivity {

    // Variables used for all the functions
    public static final String TEAM_NAME = "teamName";
    public static final String PMO_NAME = "pmoName";
    TextView textTeamName, textPMOName;
    Button buttonAddTeamMember, buttonAddProject;
    DatabaseReference databaseMembers, databaseProjects;
    ListView listViewTeamMembers, listViewProjects;
    MyApplication m = MyApplication.getInstance();
    List<Member> memberList;
    List<Project> projectList;
    String teamName, pmoName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_info);
        getSupportActionBar().setTitle("Team Information");

        // Get content from layouts
        textTeamName = findViewById(R.id.textTeamName);
        textPMOName = findViewById(R.id.textPMOName);
        buttonAddTeamMember = findViewById(R.id.buttonAddTeamMember);
        buttonAddProject = findViewById(R.id.buttonAddProject);
        listViewTeamMembers = findViewById(R.id.listViewTeamMembers);
        listViewProjects = findViewById(R.id.listViewProjects);

        // get variables from previous activity
        Intent intent = getIntent();
        teamName = intent.getStringExtra(MemberInfo.TEAM_NAME);
        pmoName = intent.getStringExtra(MemberInfo.PMO_NAME);

        // get firebase reference and objects for application
        databaseMembers = FirebaseDatabase.getInstance().getReference("Member");
        databaseProjects = FirebaseDatabase.getInstance().getReference("Project");
        memberList = new ArrayList<Member>();
        projectList = new ArrayList<Project>();

        // set the content of activity
        textTeamName.setText(teamName);
        textPMOName.setText("PMO: " + pmoName);

        // set visibility based on the role of the logged in member
        if (m.getLoggedInMember().getUsername().compareTo(pmoName) != 0) {
            buttonAddTeamMember.setVisibility(View.INVISIBLE);
            buttonAddProject.setVisibility(View.INVISIBLE);
        }

        // button to add more members to the team
        buttonAddTeamMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAddTeamMember(teamName);
            }
        });
        // button to add project to currently logged in member's team
        buttonAddProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAddProject(teamName);
            }
        });
        // click listener that can select project associated with team
        listViewProjects.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Project project = projectList.get(position);

                m.setSelectedProject(project);
                openProjectInfo();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        // read in projects from firebase
        databaseProjects.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                projectList.clear();

                for (DataSnapshot projectSnapshot : dataSnapshot.getChildren()) {

                    // get projects that matches the team name
                    Project project = projectSnapshot.getValue(Project.class);
                    if (project.getTeamName().compareTo(teamName) == 0) {
                        projectList.add(project);
                    }
                }

                ProjectList adapter = new ProjectList(TeamInfo.this, projectList);
                listViewProjects.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        // get memebers from firebase
        databaseMembers.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                memberList.clear();

                for (DataSnapshot memberSnapshot : dataSnapshot.getChildren()) {

                    // get all members with matching team name
                    Member member = memberSnapshot.getValue(Member.class);
                    if (member.getTeamList() != null) {
                        for (String x : member.getTeamList()) {
                            if (x.compareTo(teamName) == 0) {
                                memberList.add(member);
                                break;
                            }
                        }
                    }

                }

                MemberList adapter = new MemberList(TeamInfo.this, memberList);
                listViewTeamMembers.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }

        });
    }

    // open new activities
    public void openAddTeamMember(String teamName) {
        Intent intent = new Intent(this, AddTeamMembers.class);
        intent.putExtra(TEAM_NAME, teamName);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public void openAddProject(String teamName) {
        Intent intent = new Intent(this, AddProject.class);
        intent.putExtra(TEAM_NAME, teamName);
        intent.putExtra(PMO_NAME, pmoName);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public void openProjectInfo() {

        Intent intent = new Intent(TeamInfo.this, ProjectInfo.class);
        intent.putExtra(TEAM_NAME, teamName);
        intent.putExtra(PMO_NAME, pmoName);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

    }
}
