package com.example.retrospective;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

// Array adapter class that helps display the list of teams
public class TeamList extends ArrayAdapter<Team>{

    // variables
    private Activity context;
    private List<Team> teamList;
    private MyApplication m = MyApplication.getInstance();

    // constructor
    public TeamList(Activity context, List<Team> teamList) {
        super(context, R.layout.list_layout, teamList);
        this.context = context;
        this.teamList = teamList;
    }

    // get the list view of the teams
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.list_layout, null, true);

        // get the content of the activity
        TextView textTeamName = (TextView) listViewItem.findViewById(R.id.textMainName);
        TextView textRoleName = (TextView) listViewItem.findViewById(R.id.textSubName);

        Team team = teamList.get(position);

        // fill in the content
        textTeamName.setText(team.getTeamName());
        // depending on logged in members role, print the result
        if (m.getLoggedInMember().getUsername().compareTo(team.getPmo()) == 0) {
            textRoleName.setText("PMO");
        }
        else {
            textRoleName.setText("Team Member");
        }

        return listViewItem;
    }
}
