package com.example.retrospective;

// Class that will handle information regarding the user stories for project
public class UserStory {
    // variables
    private String task;
    private String assignedMember;
    private int relativeEstimate;
    private int estimateHours;
    private int actualHours;
    private int sprintPhase;
    private String status;

    // getters and setters
    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public String getAssignedMember() {
        return assignedMember;
    }

    public void setAssignedMember(String assignedMember) {
        this.assignedMember = assignedMember;
    }

    public int getRelativeEstimate() {
        return relativeEstimate;
    }

    public void setRelativeEstimate(int relativeEstimate) {
        this.relativeEstimate = relativeEstimate;
    }

    public int getEstimateHours() {
        return estimateHours;
    }

    public void setEstimateHours(int estimateHours) {
        this.estimateHours = estimateHours;
    }

    public int getActualHours() {
        return actualHours;
    }

    public void setActualHours(int actualHours) {
        this.actualHours = actualHours;
    }

    public int getSprintPhase() {
        return sprintPhase;
    }

    public void setSprintPhase(int sprintPhase) {
        this.sprintPhase = sprintPhase;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    // returns the value for how much progress was made for user story
    public int getProgress() {
        double progress = (double)estimateHours / (double)actualHours * 100.0;
        return (int)progress;
    }
}
