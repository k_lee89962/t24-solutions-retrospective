package com.example.retrospective;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class UserStoryInfo extends AppCompatActivity {

    // Variables used for all the functions
    public static final String TEAM_NAME = "teamName";
    public static final String PMO_NAME = "pmoName";
    EditText editTextTask, editTextAssignedMember, editTextRelativeEstimate, editTextEstimateHours, editTextActualHours;
    TextView textComplete, textSprintPhase;
    Button buttonChangeStatus, buttonChangeSprint, buttonApplyChanges;
    DatabaseReference databaseProject, databaseTeam;
    MyApplication m = MyApplication.getInstance();
    String teamName, pmoName, position;
    List<UserStory> userStoryList;
    List<Member> memberList;
    int usPos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_story_info);
        getSupportActionBar().setTitle("User Story Information");

        // Get content from layouts
        editTextTask = findViewById(R.id.editTextTask);
        editTextAssignedMember = findViewById(R.id.editTextAssignedMember);
        editTextRelativeEstimate = findViewById(R.id.editTextRelativeEstimate);
        editTextEstimateHours = findViewById(R.id.editTextEstimateHours);
        editTextActualHours = findViewById(R.id.editTextActualHours);
        textComplete = findViewById(R.id.textComplete);
        textSprintPhase = findViewById(R.id.textSprintPhase);
        buttonChangeStatus = findViewById(R.id.buttonChangeStatus);
        buttonChangeSprint = findViewById(R.id.buttonChangeSprint);
        buttonApplyChanges = findViewById(R.id.buttonApplyChanges);

        // get firebase reference and objects for application
        databaseProject = FirebaseDatabase.getInstance().getReference("Project");
        databaseTeam = FirebaseDatabase.getInstance().getReference("Team");
        Project project = m.getSelectedProject();
        userStoryList = new ArrayList<UserStory>();

        // get variables from previous activity
        Intent intent = getIntent();
        teamName = intent.getStringExtra(ProjectInfo.TEAM_NAME);
        pmoName = intent.getStringExtra(ProjectInfo.PMO_NAME);
        position = intent.getStringExtra(ProjectInfo.POSITION);

        // set the content of activity
        usPos = Integer.parseInt(position);
        final UserStory selectedUs = project.getUserStories().get(usPos);
        editTextTask.setText(selectedUs.getTask());
        editTextAssignedMember.setText(selectedUs.getAssignedMember());
        editTextRelativeEstimate.setText(String.valueOf(selectedUs.getRelativeEstimate()));
        editTextEstimateHours.setText(String.valueOf(selectedUs.getEstimateHours()));
        editTextActualHours.setText(String.valueOf(selectedUs.getActualHours()));
        textComplete.setText(selectedUs.getStatus());
        textSprintPhase.setText("Sprint " + selectedUs.getSprintPhase());

        // button to change the status of user story
        buttonChangeStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textComplete.getText().toString().trim().compareTo("ONGOING") == 0) textComplete.setText("COMPLETE");
                else textComplete.setText("ONGOING");
            }
        });

        // button to change the sprint of user story
        buttonChangeSprint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedUs.setSprintPhase(selectedUs.getSprintPhase() + 1);
                textSprintPhase.setText("Sprint " + selectedUs.getSprintPhase());
            }
        });

        // button to apply all the changes made in this activity, and save to firebase
        buttonApplyChanges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // if the inputs are empty
                if (editTextTask.getText().toString().trim().isEmpty() ||
                    editTextAssignedMember.getText().toString().trim().isEmpty() ||
                    editTextRelativeEstimate.getText().toString().trim().isEmpty() ||
                    editTextEstimateHours.getText().toString().trim().isEmpty() ||
                    editTextActualHours.getText().toString().trim().isEmpty()) {

                    printErrorMessage("Some of the inputs are empty.");
                }
                // if inputs are in wrong format (not integer)
                else if (!isInt(editTextRelativeEstimate.getText().toString().trim()) ||
                            !isInt(editTextEstimateHours.getText().toString().trim()) ||
                            !isInt(editTextActualHours.getText().toString().trim())) {

                    printErrorMessage("Some inputs are in wrong format.");
                }
                else {
                    // find if the member assigned exists in firebase
                    boolean found = false;
                    for (Member x : memberList) {
                        if (x.getUsername().compareTo(editTextAssignedMember.getText().toString().trim()) == 0) {
                            found = true;
                            break;
                        }
                    }
                    // member does exist, then set everything into firebase
                    if (found) {
                        m.getSelectedProject().getUserStories().get(usPos).setTask(editTextTask.getText().toString().trim());
                        m.getSelectedProject().getUserStories().get(usPos).setAssignedMember(editTextAssignedMember.getText().toString().trim());
                        m.getSelectedProject().getUserStories().get(usPos).setRelativeEstimate(Integer.parseInt(editTextRelativeEstimate.getText().toString().trim()));
                        m.getSelectedProject().getUserStories().get(usPos).setEstimateHours(Integer.parseInt(editTextEstimateHours.getText().toString().trim()));
                        m.getSelectedProject().getUserStories().get(usPos).setActualHours(Integer.parseInt(editTextActualHours.getText().toString().trim()));
                        m.getSelectedProject().getUserStories().get(usPos).setStatus(textComplete.getText().toString().trim());
                        databaseProject.child(m.getSelectedProject().getProjectName()).setValue(m.getSelectedProject());
                        openProjectInfo();
                    }
                    // member does not exist within the team, print error
                    else {
                        printErrorMessage("That member does not exist within your team.");
                    }
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        // read in teams from firebase
        databaseTeam.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot teamSnapshot : dataSnapshot.getChildren()) {
                    // select a team that matches the selected project's team name, and get the memberlist from that
                    Team team = teamSnapshot.getValue(Team.class);
                    if (team.getTeamName().compareTo(m.getSelectedProject().getTeamName()) == 0) {
                        memberList = team.getTeamMembers();
                        break;
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        // read in projects from firebase
        databaseProject.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                userStoryList.clear();

                for (DataSnapshot userStorySnapshot : dataSnapshot.getChildren()) {
                    Project project = userStorySnapshot.getValue(Project.class);

                    // select user stories from matching project
                    if (project.getTeamName().compareTo(m.getSelectedProject().getTeamName()) == 0 && project.getUserStories() != null) {
                        for (UserStory x : project.getUserStories()) {
                            userStoryList.add(x);
                        }
                        break;
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    // checks if the string is integer format
    public boolean isInt(String num) {
        if (num == null) return false;
        try {
            int i = Integer.parseInt(num);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    // error message
    public void printErrorMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    // open new activity
    public void openProjectInfo() {
        Toast.makeText(this, "Change Applied", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(UserStoryInfo.this, ProjectInfo.class);
        intent.putExtra(TEAM_NAME, teamName);
        intent.putExtra(PMO_NAME, pmoName);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
}
