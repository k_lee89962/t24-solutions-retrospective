package com.example.retrospective;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

// Array adapter class that helps display the list of members on activity
public class UserStoryList extends ArrayAdapter<UserStory> {

    // variables
    private Activity context;
    private List<UserStory> userStoryList;

    // constructor
    public UserStoryList(Activity context, List<UserStory> userStoryList) {
        super(context, R.layout.list_layout, userStoryList);
        this.context = context;
        this.userStoryList = userStoryList;
    }

    // get the list view of the user stories
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.list_layout, null, true);

        // get the content of the activity
        TextView textUserStory = listViewItem.findViewById(R.id.textMainName);
        TextView textUserStoryInfo = listViewItem.findViewById(R.id.textSubName);

        UserStory us = userStoryList.get(position);

        // fill in the content
        textUserStory.setText(us.getTask());
        textUserStoryInfo.setText("Estimate: " + us.getRelativeEstimate() + "\tEstimate Hours: " + us.getEstimateHours());

        return listViewItem;
    }
}
